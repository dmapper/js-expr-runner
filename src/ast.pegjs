{
  var TYPES_TO_PROPERTY_NAMES = {
    CallExpression:   "callee",
    MemberExpression: "object",
  };

  function filledArray(count, value) {
    var result = new Array(count), i;

    for (i = 0; i < count; i++) {
      result[i] = value;
    }

    return result;
  }

  function extractOptional(optional, index) {
    return optional ? optional[index] : null;
  }

  function extractList(list, index) {
    var result = new Array(list.length), i;

    for (i = 0; i < list.length; i++) {
      result[i] = list[i][index];
    }

    return result;
  }

  function buildList(head, tail, index) {
    return [head].concat(extractList(tail, index));
  }

  function buildTree(head, tail, builder) {
    var result = head, i;

    for (i = 0; i < tail.length; i++) {
      result = builder(result, tail[i]);
    }

    return result;
  }

  function buildBinaryExpression(head, tail, loc) {
    return buildTree(head, tail, function(result, element) {
      return {
        type:     "BinaryExpression",
        operator: element[1],
        left:     result,
        right:    element[3],
        loc
      };
    });
  }

  function optionalList(value) {
    return value !== null ? value : [];
  }
}

Start
  = __ expression:Expression __ { return { type: 'ExpressionStatement', expression, loc: location() }; }

/* ----- A.1 Lexical Grammar ----- */

SourceCharacter
  = .

WhiteSpace "whitespace"
  = "\t"
  / "\v"
  / "\f"
  / " "
  / "\u00A0"
  / "\uFEFF"
  / Zs

LineTerminator
  = [\n\r\u2028\u2029]

LineTerminatorSequence "end of line"
  = "\n"
  / "\r\n"
  / "\r"
  / "\u2028"
  / "\u2029"

Comment "comment"
  = SingleLineComment

SingleLineComment
  = "//" (!LineTerminator SourceCharacter)*

Identifier
  = !ReservedWord name:IdentifierName { return name; }

IdentifierName "identifier"
  = head:IdentifierStart tail:IdentifierPart* {
      return {
        type: "Identifier",
        name: head + tail.join(""),
        loc: location()
      };
    }

IdentifierStart
  = [a-zA-Z]
  / "$"
  / "_"

IdentifierPart
  = IdentifierStart
  / DecimalDigit

ReservedWord
  = NullLiteral
  / UndefinedLiteral
  / BooleanLiteral

Literal
  = NullLiteral
  / UndefinedLiteral
  / BooleanLiteral
  / NumericLiteral
  / StringLiteral
  / RegularExpressionLiteral

NullLiteral
  = NullToken { return { type: "Literal", value: null, loc: location() }; }

UndefinedLiteral
  = UndefinedToken { return { type: "Literal", value: undefined, loc: location() }; }

BooleanLiteral
  = TrueToken  { return { type: "Literal", value: true, loc: location()  }; }
  / FalseToken { return { type: "Literal", value: false, loc: location() }; }

NumericLiteral "number"
  = literal:DecimalLiteral !(IdentifierStart / DecimalDigit) {
      return literal;
    }

DecimalLiteral
  = DecimalIntegerLiteral "." DecimalDigit* ExponentPart? {
      return { type: "Literal", value: parseFloat(text()), loc: location() };
    }
  / "." DecimalDigit+ ExponentPart? {
      return { type: "Literal", value: parseFloat(text()), loc: location() };
    }
  / DecimalIntegerLiteral ExponentPart? {
      return { type: "Literal", value: parseFloat(text()), loc: location()  };
    }

DecimalIntegerLiteral
  = "0"
  / NonZeroDigit DecimalDigit*

DecimalDigit
  = [0-9]

NonZeroDigit
  = [1-9]

ExponentPart
  = ExponentIndicator SignedInteger

ExponentIndicator
  = "e"i

SignedInteger
  = [+-]? DecimalDigit+

HexIntegerLiteral
  = "0x"i digits:$HexDigit+ {
      return { type: "Literal", value: parseInt(digits, 16), loc: location() };
     }

HexDigit
  = [0-9a-f]i

StringLiteral "string"
  = '"' chars:DoubleStringCharacter* '"' {
      return { type: "Literal", value: chars.join(""), loc: location() };
    }
  / "'" chars:SingleStringCharacter* "'" {
      return { type: "Literal", value: chars.join(""), loc: location() };
    }

DoubleStringCharacter
  = !('"' / "\\" / LineTerminator) SourceCharacter { return text(); }
  / "\\" sequence:EscapeSequence { return sequence; }
  / LineContinuation

SingleStringCharacter
  = !("'" / "\\" / LineTerminator) SourceCharacter { return text(); }
  / "\\" sequence:EscapeSequence { return sequence; }
  / LineContinuation

LineContinuation
  = "\\" LineTerminatorSequence { return ""; }

EscapeSequence
  = CharacterEscapeSequence
  / "0" !DecimalDigit { return "\0"; }

CharacterEscapeSequence
  = SingleEscapeCharacter
  / NonEscapeCharacter

SingleEscapeCharacter
  = "'"
  / '"'
  / "\\"
  / "b"  { return "\b";   }
  / "f"  { return "\f";   }
  / "n"  { return "\n";   }
  / "r"  { return "\r";   }
  / "t"  { return "\t";   }
  / "v"  { return "\x0B"; }   // IE does not recognize "\v".

NonEscapeCharacter
  = !(EscapeCharacter / LineTerminator) SourceCharacter { return text(); }

EscapeCharacter
  = SingleEscapeCharacter
  / DecimalDigit
  / "u"

RegularExpressionLiteral "regular expression"
  = "/" pattern:$RegularExpressionBody "/" flags:$RegularExpressionFlags {
      var value;

      try {
        value = new RegExp(pattern, flags);
      } catch (e) {
        error(e.message);
      }

      return { type: "Literal", value: value, loc: location() };
    }

RegularExpressionBody
  = RegularExpressionFirstChar RegularExpressionChar*

RegularExpressionFirstChar
  = ![*\\/[] RegularExpressionNonTerminator
  / RegularExpressionBackslashSequence
  / RegularExpressionClass

RegularExpressionChar
  = ![\\/[] RegularExpressionNonTerminator
  / RegularExpressionBackslashSequence
  / RegularExpressionClass

RegularExpressionBackslashSequence
  = "\\" RegularExpressionNonTerminator

RegularExpressionNonTerminator
  = !LineTerminator SourceCharacter

RegularExpressionClass
  = "[" RegularExpressionClassChar* "]"

RegularExpressionClassChar
  = ![\]\\] RegularExpressionNonTerminator
  / RegularExpressionBackslashSequence

RegularExpressionFlags
  = IdentifierPart*

// [Unicode] Separator, Space
Zs = [\u0020\u00A0\u1680\u2000-\u200A\u202F\u205F\u3000]

/* Tokens */

FalseToken      = "false"      !IdentifierPart
NullToken       = "null"       !IdentifierPart
UndefinedToken  = "undefined"  !IdentifierPart
TrueToken       = "true"       !IdentifierPart

/* Skipped */

__
  = (WhiteSpace / LineTerminatorSequence / Comment)*

_
  = (WhiteSpace)*

/* Automatic Semicolon Insertion */

EOS
  = _ SingleLineComment? LineTerminatorSequence
  / _ &"}"
  / __ EOF

EOF
  = !.

/* ----- A.2 Number Conversions ----- */

/* Irrelevant. */

/* ----- A.3 Expressions ----- */

PrimaryExpression
  = Identifier
  / Literal
  / ArrayLiteral
  / ObjectLiteral
  / "(" __ expression:Expression __ ")" { return expression; }

ArrayLiteral
  = "[" __ elision:(Elision __)? "]" {
      return {
        type:     "ArrayExpression",
        elements: optionalList(extractOptional(elision, 0)),
        loc: location()
      };
    }
  / "[" __ elements:ElementList __ "]" {
      return {
        type:     "ArrayExpression",
        elements: elements,
        loc: location()
      };
    }
  / "[" __ elements:ElementList __ "," __ elision:(Elision __)? "]" {
      return {
        type:     "ArrayExpression",
        elements: elements.concat(optionalList(extractOptional(elision, 0))),
        loc: location()
      };
    }

ElementList
  = head:(
      elision:(Elision __)? element:ConditionalExpression {
        return optionalList(extractOptional(elision, 0)).concat(element);
      }
      /
      elision:(Elision __)? element:SpreadElement {
        return optionalList(extractOptional(elision, 0)).concat(element);
      }

	)
    tail:(
      __ "," __ elision:(Elision __)? element:ConditionalExpression {
        return optionalList(extractOptional(elision, 0)).concat(element);
      }
      /
      __ "," __ elision:(Elision __)? element:SpreadElement {
        return optionalList(extractOptional(elision, 0)).concat(element);
      }
	)*
    { return Array.prototype.concat.apply(head, tail); }

SpreadElement
  = "..." __ element:ConditionalExpression {
    return ({
      type: "SpreadElement",
      argument: element,
      loc: location()
    });
  }

Elision
  = "," commas:(__ ",")* { return filledArray(commas.length + 1, null); }

ObjectLiteral
  = "{" __ "}" { return { type: "ObjectExpression", properties: [] }; }
  / "{" __ properties:PropertyNameAndValueList __ "}" {
       return { type: "ObjectExpression", properties: properties, loc: location() };
     }
  / "{" __ properties:PropertyNameAndValueList __ "," __ "}" {
       return { type: "ObjectExpression", properties: properties, loc: location() };
     }

PropertyNameAndValueList
  = head:PropertyAssignment tail:(__ "," __ PropertyAssignment)* {
      return buildList(head, tail, 3);
    }

PropertyAssignment
  = key:PropertyName __ ":" __ value:ConditionalExpression {
      return { key: key, value: value };
    }

PropertyName
  = IdentifierName
  / StringLiteral
  / NumericLiteral

MemberExpression
  = head:(
        PrimaryExpression
    )
    tail:(
        __ "[" __ property:Expression __ "]" {
          return { property: property, computed: true };
        }
      / __ "?." __ "[" __ property:Expression __ "]" {
          return { property: property, computed: true, optionalChaining: true };
        }

      / __ "." __ property:Identifier {
          return { property: property, computed: false };
        }
      / __ "?." __ property:Identifier {
          return { property: property, computed: false, optionalChaining: true };
        }
    )*
    {
      return buildTree(head, tail, function(result, element) {
        var result = {
          type:     "MemberExpression",
          object:   result,
          property: element.property,
          computed: element.computed,
          loc: location()
        }
        if (element.optionalChaining) {
		  result.optionalChaining = element.optionalChaining
        }
        return result
      });
    }

CallExpression
  = head:(
      callee:MemberExpression __ args:Arguments {
        var result = { type: "CallExpression", callee: callee, arguments: args.arguments, loc: location() }
        if (args.optionalChaining) {
		  result.optionalChaining = args.optionalChaining
        }
        return result;
      }
    )
    tail:(
        __ args:Arguments {
        var result = { type: "CallExpression", arguments: args.arguments, loc: location() }
        if (args.optionalChaining) {
		  result.optionalChaining = args.optionalChaining
        }
        return result;
      }
      / __ "?." __ "[" __ property:Expression __ "]" {
          return {
            type:     "MemberExpression",
            property: property,
            computed: true,
            optionalChaining: true,
            loc: location()
          };
        }
      / __ "[" __ property:Expression __ "]" {
          return {
            type:     "MemberExpression",
            property: property,
            computed: true,
            loc: location()
          };
        }
      / __ "?." __ property:IdentifierName {
          return {
            type:     "MemberExpression",
            property: property,
            computed: false,
            optionalChaining: true,
            loc: location()
          };
        }
      / __ "." __ property:IdentifierName {
          return {
            type:     "MemberExpression",
            property: property,
            computed: false,
            loc: location()
          };
        }
    )*
    {
      return buildTree(head, tail, function(result, element) {
        element[TYPES_TO_PROPERTY_NAMES[element.type]] = result;

        return element;
      });
    }

Arguments
  = "(" __ args:(ArgumentList __)? ")" {
      return {arguments: optionalList(extractOptional(args, 0))};
    }
	/ "?." __ "(" __ args:(ArgumentList __)? ")" {
      return {arguments: optionalList(extractOptional(args, 0)), optionalChaining: true};
    }

ArgumentList
  = head:ArgumentItem tail:(__ "," __ ArgumentItem)* {
      return buildList(head, tail, 3);
    }

ArgumentItem
  = ConditionalExpression
  / SpreadElement

LeftHandSideExpression
  = CallExpression
  / MemberExpression

// TODO: Maybe it should have right recursion here
UnaryExpression
  = LeftHandSideExpression
  / operator:UnaryOperator __ argument:UnaryExpression {
      return {
        type:     "UnaryExpression",
        operator: operator,
        argument: argument,
        prefix:   true,
        loc: location()
      };
    }

UnaryOperator
  = $("+" !"=")
  / $("-" !"=")
  / "!"

MultiplicativeExpression
  = head:UnaryExpression
    tail:(__ MultiplicativeOperator __ UnaryExpression)*
    { return buildBinaryExpression(head, tail, location()); }

MultiplicativeOperator
  = $("*" !"=")
  / $("/" !"=")
  / $("%" !"=")

AdditiveExpression
  = head:MultiplicativeExpression
    tail:(__ AdditiveOperator __ MultiplicativeExpression)*
    { return buildBinaryExpression(head, tail, location()); }

AdditiveOperator
  = $("+" ![+=])
  / $("-" ![-=])

RelationalExpression
  = head:AdditiveExpression
    tail:(__ RelationalOperator __ AdditiveExpression)*
    { return buildBinaryExpression(head, tail, location()); }

RelationalOperator
  = "<="
  / ">="
  / $("<" !"<")
  / $(">" !">")

EqualityExpression
  = head:RelationalExpression
    tail:(__ EqualityOperator __ RelationalExpression)*
    { return buildBinaryExpression(head, tail, location()); }

EqualityOperator
  = "==="
  / "!=="
  / "=="
  / "!="

LogicalANDExpression
  = head:EqualityExpression
    tail:(__ LogicalANDOperator __ EqualityExpression)*
    { return buildBinaryExpression(head, tail, location()); }

LogicalANDOperator
  = "&&"

LogicalORExpression
  = head:LogicalANDExpression
    tail:(__ LogicalOROperator __ LogicalANDExpression)*
    { return buildBinaryExpression(head, tail, location()); }

LogicalOROperator
  = "||"

NullishCoalescingExpression
  = head:LogicalORExpression
    tail:(__ NullishCoalescingOperator __ LogicalORExpression)*
    { return buildBinaryExpression(head, tail, location()); }

NullishCoalescingOperator
  = "??"

ConditionalExpression
  = test:NullishCoalescingExpression __
    "?" __ consequent:ConditionalExpression __
    ":" __ alternate:ConditionalExpression
    {
      return {
        type:       "ConditionalExpression",
        test:       test,
        consequent: consequent,
        alternate:  alternate,
        loc: location()
      };
    }
  / NullishCoalescingExpression

Expression
  = head:ConditionalExpression tail:(__ "," __ ConditionalExpression)* {
      return tail.length > 0
        ? { type: "SequenceExpression", expressions: buildList(head, tail, 3), loc: location() }
        : head;
    }
