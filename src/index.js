const ast = require('./ast')
const fabric = require('./runner/fabric')
const { isFunction, isPlainObject, isArray, isString } = require('lodash')
const isNode = node =>  isPlainObject(node) && node.type
const getNodeType = node => node.type

class Runner {
  constructor (expression) {
    if (isString(expression)) {
      this.astObj = ast.parse(expression)
    } else {
      this.astObj = expression
    }

    this.ast = fabric(this.astObj)
  }

  run (context = {}, options) {
    return this.ast.get(context, options)
  }

  getPureAst() {
    return this.astObj
  }

  traverse(callbackMap) {
    function visit(node, parent, key, index) {
      if (isFunction(callbackMap)) {
        callbackMap(node, parent, key, index)
      }
      if (isPlainObject(callbackMap)) {
        const nodeType = getNodeType(node)
        if (nodeType in callbackMap) {
          callbackMap[nodeType](node, parent, key, index)
        }
      }

      const keys = Object.keys(node)
      for (const key of keys) {
        const child = node[key]
        if (isArray(child)) {
          for (let index = 0; index < child.length; index++) {
            visit(child[index], node, key, index)
          }
        } else if (isNode(child)) {
          visit(child, node, key)
        }
      }
    }
    visit(this.astObj, null)
  }

  // getFields() {
  //   const fields = []
  //   this.traverse({Identifier: (node, parent, key, index) => {
  //     if (parent) {
  //       if (parent.type === 'ExpressionStatement') {
  //         fields.push(node.name)
  //       }
  //       if (parent.type === 'BinaryExpression') {
  //         fields.push(node.name)
  //       }
  //       if (parent.type === 'MemberExpression' && key === 'object') {
  //         fields.push(node.name)
  //       }
  //       if (parent.type === 'MemberExpression' && key === 'property' && parent.computed) {
  //         fields.push(node.name)
  //       }
  //     }
  //   }})
  //   return fields
  // }

  getFields() {
    const fields = []

    this.ast.getFields(fields)

    return fields
  }
}

module.exports = {
  getAst: ast.parse,
  getRunner: (expressionString) => new Runner(expressionString)
}
