const ExpressionBase = require('./ExpressionBase')
const unaryOperatorFns = require('./operators/unary')

module.exports = class UnaryExpression extends ExpressionBase{
  constructor ({operator, argument, prefix}) {
    super()
    this.prefix = prefix
    this.operator = operator
    this.argument = this.wrapAst(argument)
  }

  get(context, options = {}) {
    const argumentResult = this.argument.get(context, options)
    const warnings = [...(argumentResult.warnings || [])]
    const value = unaryOperatorFns[this.operator](argumentResult.value)

    return {
      shortCircuit: false,
      value,
      warnings,
    }
  }
  getFields(fields) {
    this._maybeAddField(fields, this.argument.getFields(fields))
  }
}
