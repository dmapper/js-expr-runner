const ExpressionBase = require('./ExpressionBase')

module.exports = class ObjectExpression extends ExpressionBase{
  constructor ({properties}) {
    super()
    this.properties = properties.map( property => {
      const {key, value} = property
      return {
        key: this.wrapAst(key),
        value: this.wrapAst(value)
      }
    })

  }

  get(context, options = {}) {
    const warnings = []
    const item = {}
    this.properties.forEach(property => {
      const propertyKeyResult = property.key.get(context, {...options, computed: false})
      warnings.push(...(propertyKeyResult.warnings || []))
      const propertyValueResult = property.value.get(context, options)
      warnings.push(...(propertyValueResult.warnings || []))
      item[propertyKeyResult.value] = propertyValueResult.value
    })

    return {
      shortCircuit: false,
      value: item,
      warnings,
    }
  }

  getFields(fields) {
    this.properties.forEach(property => {
      this._maybeAddField(fields, property.value.getFields(fields))
    })
  }
}
