const ExpressionBase = require('./ExpressionBase')
const binaryOperatorFns = require('./operators/binary')

module.exports = class BinaryExpression extends ExpressionBase{
  constructor ({left, right, operator}) {
    super()
    this.operator = operator
    this.left = this.wrapAst(left)
    this.right = this.wrapAst(right)
  }

  get(context, options = {}) {
    const warnings = []
    const leftResult = this.left.get(context, options)
    warnings.push(...(leftResult.warnings || []))
    const left = leftResult.value

    const lazyRightFn = () => {
      const rightResult = this.right.get(context, options)
      warnings.push(...(rightResult.warnings || []))
      return rightResult.value
    }

    const operatorFn = binaryOperatorFns[this.operator]

    if (!operatorFn) {
      throw new Error(`Unknown operator ${this.operator}`)
    }

    return {
      shortCircuit: false,
      value: operatorFn(left, lazyRightFn),
      warnings,
    }
  }

  getFields(fields) {
    this._maybeAddField(fields, this.left.getFields(fields))
    this._maybeAddField(fields, this.right.getFields(fields))
  }
}
