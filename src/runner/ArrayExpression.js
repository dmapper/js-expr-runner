const ExpressionBase = require('./ExpressionBase')

module.exports = class ArrayExpression extends ExpressionBase{
  constructor ({elements}) {
    super()
    this.elements = elements.map( element => {
      return element ? this.wrapAst(element) : null
    })

  }

  //a = [...a, ...b, 2]
  //a = [].concat(a, b, [2]);
  get(context, options = {}) {
    const warnings = []
    const value = [].concat(...(this.elements.map(element => {
      if (!element) return null

      const result = element.get(context, options)
      warnings.push(...(result.warnings || []))
      return result.spread ? result.value : [result.value]
    })))

    return {
      shortCircuit: false,
      value,
      warnings,
    }
  }

  getFields(fields) {
    this.elements.forEach(element => {
      this._maybeAddField(fields, element.getFields(fields))
    })
  }
}
