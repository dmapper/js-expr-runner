const ExpressionBase = require('./ExpressionBase')

module.exports = class SpreadElement extends ExpressionBase{
  constructor ({argument}) {
    super()
    this.argument = this.wrapAst(argument)
  }

  get(context, options = {}) {
    const valueResult = this.argument.get(context, options)
    const warnings = [...(valueResult.warnings || [])]
    return ({
      shortCircuit: false,
      spread: true,
      value: valueResult.value,
      warnings,
    })
  }

  getFields(fields) {
    this._maybeAddField(fields, this.argument.getFields(fields))
  }
}
