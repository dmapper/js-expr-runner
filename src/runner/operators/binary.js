module.exports = {
  '+': (left, lazyRightFn) => left + lazyRightFn(),
  '-': (left, lazyRightFn) => left - lazyRightFn(),
  '*': (left, lazyRightFn) => left * lazyRightFn(),
  '/': (left, lazyRightFn) => left / lazyRightFn(),
  '%': (left, lazyRightFn) => left % lazyRightFn(),

  '<=': (left, lazyRightFn) => left <= lazyRightFn(),
  '>=': (left, lazyRightFn) => left >= lazyRightFn(),
  '<': (left, lazyRightFn) => left < lazyRightFn(),
  '>': (left, lazyRightFn) => left > lazyRightFn(),
  '===': (left, lazyRightFn) => left === lazyRightFn(),
  '!==': (left, lazyRightFn) => left !== lazyRightFn(),
  '==': (left, lazyRightFn) => left == lazyRightFn(),
  '!=': (left, lazyRightFn) => left != lazyRightFn(),
  '||': (left, lazyRightFn) => left || lazyRightFn(),
  '&&': (left, lazyRightFn) => left && lazyRightFn(),
  '??': (left, lazyRightFn) => {
    if (left !== null && left !== undefined) return left
    return lazyRightFn()
  },
}
