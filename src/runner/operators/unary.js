module.exports = {
  '-': value => -value,
  '+': value => value,
  '!': value => !value,
}
