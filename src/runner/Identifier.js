const ExpressionBase = require('./ExpressionBase')
const binaryOperatorFns = require('./operators/binary')

module.exports = class Identifier extends ExpressionBase{
  constructor ({name, loc}) {
    super()
    this.name = name
    this.loc = {start: loc.start.offset, end: loc.end.offset }
  }

  get(context, options = {}) {
    const { computed = true } = options
    const warnings = []
    const value = computed ? context[this.name] : this.name
    return {
      shortCircuit: false,
      value,
      warnings,
    }
  }

  getFields() {
    return [{segment: this.name, loc: this.loc}]
  }
}
