const ExpressionBase = require('./ExpressionBase')

module.exports = class ConditionalExpression extends ExpressionBase{
  constructor ({test, consequent, alternate}) {
    super()
    this.test = this.wrapAst(test)
    this.consequent = this.wrapAst(consequent)
    this.alternate = this.wrapAst(alternate)
  }

  get(context, options = {}) {
    const warnings = []
    const testResult = this.test.get(context, options)
    warnings.push(...(testResult.warnings || []))
    const calcConsequent = () => {
      const consequentResult = this.consequent.get(context, options)
      warnings.push(...(consequentResult.warnings || []))
      return consequentResult.value
    }

    const calcAlternate = () => {
      const alternateResult = this.alternate.get(context, options)
      warnings.push(...(alternateResult.warnings || []))
      return alternateResult.value
    }

    const value = testResult.value ? calcConsequent() : calcAlternate()

    return ({
      shortCircuit: false,
      value,
      warnings,
    })
  }

  getFields(fields) {
    this._maybeAddField(fields, this.test.getFields(fields))
    this._maybeAddField(fields, this.consequent.getFields(fields))
    this._maybeAddField(fields, this.alternate.getFields(fields))
  }
}
