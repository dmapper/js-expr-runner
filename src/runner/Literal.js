const ExpressionBase = require('./ExpressionBase')

module.exports = class Literal extends ExpressionBase{
  constructor ({value}) {
    super()
    this.value = value
  }

  get() {
    return {
      shortCircuit: false,
      value: this.value,
      warnings: []
    }
  }
}
