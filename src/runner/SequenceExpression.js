const ExpressionBase = require('./ExpressionBase')

module.exports = class SequenceExpression extends ExpressionBase{
  constructor ({expressions} ) {
    super()
    this.expressions = expressions.map(exp => this.wrapAst(exp))
  }

  get(context, options = {}) {
    const warnings = []
    const result = this.expressions.map(exp => {
      const expressionResult = exp.get(context, options)
      warnings.push(...(expressionResult.warnings || []))
      return expressionResult.value
    })
    const value = result[result.length - 1]

    return ({
      shortCircuit: false,
      value,
      warnings,
    })
  }

  getFields(fields) {
    this.expressions.forEach(exp => {
      this._maybeAddField(fields, exp.getFields(fields))
    })
  }  
}
