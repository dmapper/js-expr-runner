const ExpressionBase = require('./ExpressionBase')
const { isArray } = require('lodash')

module.exports = class MemberExpression extends ExpressionBase{
  constructor ({object, property, computed, optionalChaining}) {
    super()
    this.computed = computed
    this.optionalChaining = optionalChaining
    this.property = this.wrapAst(property)
    this.object = this.wrapAst(object)
  }

  get(context, options = {}) {
    const object = this.object.get(context, options)
    const warnings = [...(object.warnings || [])]
    if (object.shortCircuit || ((this.optionalChaining || options.alwaysOptionalChaining) && object.value == null)) {

      if (!this.optionalChaining && options.alwaysOptionalChaining && object.value == null) {
        warnings.push(`MemberExpression: error in chaining`)
      }

      return ({
        shortCircuit: true,
        value: object.value,
        warnings,
      })
    }

    const keyResult = this.property.get(context, {...options, computed: this.computed})
    warnings.push(...(keyResult.warnings || []))

    const res = this.tryCatchWrapper(options, 'CallExpression', warnings, () => {
      return object.value[keyResult.value]
    })
    return ({
      shortCircuit: false,
      self: object.value,
      ...res
    })
  }

  getFields(fields) {
    if (this.computed) {
      this._maybeAddField(fields, this.object.getFields(fields))
      this._maybeAddField(fields, this.property.getFields(fields))
      return
    }

    const objectResult = this.object.getFields(fields)
    const propertyResult = this.property.getFields(fields)

    if (isArray(objectResult) && isArray(propertyResult)) {
      return [...objectResult, ...propertyResult]
    }

  }
}
