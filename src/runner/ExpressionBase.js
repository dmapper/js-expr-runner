module.exports = class ExpressionBase {
  constructor () {}

  wrapAst(ast) {
    const fabric = require('./fabric')
    return fabric(ast)
  }

  getFields() {

  }

  _maybeAddField(fields, fieldResult) {
    if (fieldResult && fieldResult.length > 0) {
      const exp = fieldResult.map(it => it.segment).join('.')
      const start = Math.min(...fieldResult.map(it => it.loc.start))
      const end = Math.max(...fieldResult.map(it => it.loc.end))

      fields.push({ exp, start, end })
    }
  }

  tryCatchWrapper(options = {}, operation, warnings, fn) {
    if (!options.dontThrowErrors) {
      return {
        value: fn(),
        warnings,
      }
    }

    let result = {
      value: undefined,
      warnings
    }

    try {
      result.value = fn()
    } catch (error) {
      result.warnings.push(`${ operation }: ${ String(error)}`)
    }

    return result
  }
}
