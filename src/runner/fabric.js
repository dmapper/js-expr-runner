const expressions = {
  Literal: require ('./Literal'),
  UnaryExpression: require('./UnaryExpression'),
  ArrayExpression: require ('./ArrayExpression'),
  ObjectExpression: require ('./ObjectExpression'),
  BinaryExpression: require ('./BinaryExpression'),
  Identifier: require ('./Identifier'),
  MemberExpression: require ('./MemberExpression'),
  CallExpression: require ('./CallExpression'),
  ConditionalExpression: require ('./ConditionalExpression'),
  SequenceExpression: require ('./SequenceExpression'),
  SpreadElement: require ('./SpreadElement'),
  ExpressionStatement: require ('./ExpressionStatement'),
}

module.exports = function (expression) {
  if (expressions[expression.type]) {
    return new expressions[expression.type](expression)
  } else {
    console.log('fabric error', expression)
  }
}
