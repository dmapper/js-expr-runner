const ExpressionBase = require('./ExpressionBase')

module.exports = class ExpressionStatement extends ExpressionBase {
  constructor ({expression}) {
    super()
    this.expression = this.wrapAst(expression)
  }

  get (context, options = {}) {
    const result = this.expression.get(context, options)

    return {
      shortCircuit: false,
      value: result.value,
      warnings: result.warnings,
    }
  }

  getFields(fields) {
    this._maybeAddField(fields, this.expression.getFields(fields), this.loc)
  }

}
