const ExpressionBase = require('./ExpressionBase')

module.exports = class CallExpression extends ExpressionBase{
  constructor ({callee, arguments: args, optionalChaining}) {
    super()
    this.optionalChaining = optionalChaining
    this.callee = this.wrapAst(callee)
    this.arguments = args.map(arg => this.wrapAst(arg))
  }

  get(context, options = {}) {
    const callee = this.callee.get(context, options)
    const warnings = [...(callee.warnings || [])]

    if (callee.shortCircuit || ((this.optionalChaining || options.alwaysOptionalChaining) && callee.value == null)) {
      if (!this.optionalChaining && options.alwaysOptionalChaining && callee.value == null) {
        warnings.push(`CallExpression: error in chaining`)
      }
      return ({
        shortCircuit: true,
        value: callee.value,
        warnings,
      })
    }

    const args = [].concat(...(this.arguments.map(arg => {
      const result = arg.get(context, options)
      warnings.push(...(result.warnings || []))
      return result.spread ? result.value : [result.value]
    })))

    const res = this.tryCatchWrapper(options, 'CallExpression', warnings, () => {
      return callee.value.call(callee.self || context, ...args)
    })
    
    return ({
      shortCircuit: false,
      ...res,
    })
  }

  getFields(fields) {
    const calleeResult = this.callee.getFields(fields)
    if (calleeResult && calleeResult.length > 0) {
      calleeResult.pop()
      this._maybeAddField(fields, calleeResult)
    }


    this.arguments.forEach(arg => {
      this._maybeAddField(fields, arg.getFields(fields))
    })
  }
}
