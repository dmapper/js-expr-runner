const Runner = require('../src')
const assert = require('assert')

// run simple expression
const e = (expression, context, result) => {
  const runner = Runner.getRunner(expression)
  // console.log('ast', JSON.stringify(runner.traverse(), null,2))
  assert.deepEqual(runner.run(context).value, result)
}

// run expression with alwaysOptionalChaining: true
// so '.' should always work as '?.'
// system should not throw errors but return them as warnings

const p = (expression, context, result, warnings) => {
  const runner = Runner.getRunner(expression)
  // console.log('ast', JSON.stringify(runner.traverse(), null,2))
  const actualResult = runner.run(context, {alwaysOptionalChaining: true})
  assert.deepEqual(actualResult.value, result)
  assert.deepEqual(actualResult.warnings, warnings)
}

const noThrow = (expression, context, result, warnings) => {
  const runner = Runner.getRunner(expression)
  const actualResult = runner.run(context, {dontThrowErrors: true})
  assert.deepEqual(actualResult.value, result)
  assert.deepEqual(actualResult.warnings, warnings)
}

// expression should throw error
const t = (expression, context) => {
  const runner = Runner.getRunner(expression)
  assert.throws(() => runner.run(context))
}

const f = (expression, fields) => {
  const runner = Runner.getRunner(expression)
  const result = runner.getFields()
  assert.deepEqual(result, fields)
}

describe('Literals', function() {
  describe('numbers', function() {
    it('0', () => e('0', {}, 0))
    it('100.5', () => e('100.5', {}, 100.5))
  })
  describe('strings', function() {
    it("'hello'", () => e(`'hello'`, {}, 'hello'))
    it('"hello"', () => e(`"hello"`, {}, 'hello'))
  })

  describe('boolean', function() {
    it('true', () => e(`true`, {}, true))
    it('false', () => e(`false`, {}, false))
  })

  describe('[]', function() {
    it("[]", () => e(`[]`, {}, []))
    it("[0]", () => e(`[0]`, {}, [0]))
    it("[1,2,3]", () => e(`[1,2,3]`, {}, [1,2,3]))
    it("[,0]", () => e(`[,0]`, {}, [null,0]))
    it("[,0,,]", () => e(`[,0,,]`, {}, [null,0, null]))
  })

  describe('{}', function() {
    it("{}", () => e(`{}`, {}, {}))
    it("{a: 1}", () => e(`{a: 1}`, {}, {a: 1}))
  })
})

describe('Unary operators', function() {
  it('-', () => e(`-1`, {}, -1))
  it('+', () => e(`+1`, {}, 1))
  it('!', () => e(`!true`, {}, false))
})

describe('Binary operators', function() {
  it('1 - 1', () => e(`1 - 1`, {}, 0))
  it('1 + 1', () => e(`1+1`, {}, 2))
  it('1 || 2', () => e(`1 || 2`, {}, 1))
  it('0 || 2', () => e(`0 || 2`, {}, 2))
  it('1 && 2', () => e(`1 && 2`, {}, 2))
  it('0 && 2', () => e(`0 && 2`, {}, 0))
  it('1 ?? 2', () => e(`1 ?? 2`, {}, 1))
  it('0 ?? 2', () => e(`0 ?? 2`, {}, 0))
  it('undefined ?? 2', () => e(`undefined ?? 2`, {}, 2))
  it('null ?? 2', () => e(`null ?? 2`, {}, 2))
})

describe('Path', function() {
  it('a', () => e(`a`, {a: 10}, 10))
  it('a.b', () => e(`a.b`, {a: {b: 10}}, 10))
  it('a.b - throws error', () => t(`a.b`, {} ))
  it('a.b - do not throw error', () => p(`a.b`, {}, undefined, ['MemberExpression: error in chaining'] ))
  it('a.b - do not throw error', () => noThrow(`a.b.c`, {}, undefined, ["CallExpression: TypeError: Cannot read property 'b' of undefined", "CallExpression: TypeError: Cannot read property 'c' of undefined"] ))
  it("a['b']", () => e(`a['b']`, {a: {b: 10}}, 10))
  it('a.b.c', () => e(`a.b.c`, {a: {b: {c: 10}}}, 10))
  it('a?.b', () => e(`a?.b`, {}, undefined))
  it('a?.b.c', () => e(`a?.b.c`, {}, undefined))
})


describe('Call', function() {
  it('a()', () => e(`a()`, {a: () => 10}, 10))
  it('a() - throw error', () => t(`a()`, {}))
  it('a() - do not throw error', () => p(`a()`, {}, undefined, ['CallExpression: error in chaining']))
  it('a() - do not throw error', () => noThrow(`a()`, {}, undefined, ["CallExpression: TypeError: Cannot read property 'call' of undefined"]))
  it('a() - do not throw error', () => noThrow(`a()`, {
    a: () => { throw new Error('My Error') }
  }, undefined, ["CallExpression: Error: My Error"]))
  it('a?.() - present in context', () => e(`a?.()`, {a: () => 10}, 10))
  it('a?.() - absent in context', () => e(`a?.()`, {}, undefined))
  it('a.b()', () => e(`a.b()`, {a: {b: () => 10}}, 10))
  it('a.b().c', () => e(`a.b().c`, {a: {b: () => ({c: 10})}}, 10))
  it('a(10)', () => e(`a(10)`, {a: n => n}, 10))
  it('sum(1,2)', () => e(`sum(1,2)`, {sum: (a, b) => a + b}, 3))
  it('Math.E', () => e(`Math.E`, {Math}, Math.E))
  describe('this', function() {
    it('a.b()', () => e(`a.b()`, {a: {b: function () { return this.c }, c: 10}}, 10))
    it('a.b()()', () => e(`a.b()()`, {
      a: {
        b: function () {
          return (function () { return this.d})
        },
        c: 10
      },
      d: 20
    }, 20))
  })
  it("a(b(0)) + a(b(0))", () => e(`a(b(0)) + c(d(0))`, {a: (n) => n, b: (n) => n, c: (n) => n, d: (n) => n, }, 0))
})

describe('Ternary ?:', function() {
  it('true ? 1 : 2', () => e(`true ? 1 : 2`, {}, 1))
  it('false ? 1 : 2', () => e(`true ? 1 : 2`, {}, 1))
})

describe('Comma ,', function() {
  it('1,2,3', () => e(`1,2,3`, {}, 3))
  it('(1,2,3)', () => e(`(1,2,3)`, {}, 3))
})

describe('Complex', function() {
  it(`125 * 10, a['b'](10, 20, 1+7*10).d`, () => e(`125 * 10, a['b'](10, 20, 1+7*10).d`, {a: {b: function(a,b,c) { return {d: a+b+c}}}}, 101))
})

describe('Array Spread', function() {
  it('[...a]', () => e(`[...a]`, {a: [1,2,3]}, [1,2,3]))
  it('[1, ...a, 2, ...a]', () => e(`[1, ...a, 2, ...a]`, {a: [1,2,3]}, [1,1,2,3,2,1,2,3]))
})

describe('Call Spread', function() {
  it('c(...a)', () => e(`c(...a)`, {a: [1,2,3], c: (...args) => args}, [1,2,3]))
  it('c(1,...a,4)', () => e(`c(1,...a,4)`, {a: [1,2,3], c: (...args) => args}, [1,1,2,3,4]))
})

describe('Security', function() {
  it('window.document - throws error', () => t(`window.document`, {}))
  it('global.console.log() - throws error', () => t(`global.console.log()`, {}))
  it('console.log() - throws error', () => t(`console.log()`, {}))
  it('window - undefined', () => e(`window`, {}, undefined))
  it('global - undefined', () => e(`global`, {}, undefined))
  it('this - undefined', () => e(`this`, {}, undefined))
})

describe.skip('Traverse', function() {
  it('traverse', () => {
    const runner = Runner.getRunner(`1`)
    const nodes = []
    runner.traverse({Literal: node => nodes.push(node)})

    assert.deepEqual(nodes, [
      { type: 'Literal', value: 1 },
    ])
  })
})

describe('Extract fields', function() {
  it('empty', () => f('1', []))
  it('a', () => f('a', [{exp: 'a', start: 0, end: 1}]))
  it('a.b', () => f('a.b', [{exp: 'a.b', start:0, end: 3}]))
  it('a?.b', () => f('a?.b', [{exp: 'a.b', start: 0, end: 4}]))
  it('2 + a.b.c', () => f('2 + a.b.c', [{exp: 'a.b.c', start: 4, end: 9}]))
  it('a+b', () => f('a+b', [{exp: 'a', start: 0, end: 1}, {exp: 'b', start: 2, end: 3}]))
  it('a+b+c', () => f('a+b+c', [{exp: 'a', start: 0, end: 1}, {exp: 'b', start: 2, end: 3}, {exp: 'c', start: 4, end: 5}]))
  it('a()', () => f('a()', []))
  it('a.b()', () => f('a.b()', [{exp: 'a', start: 0, end: 1}]))
  it('a(b.c)', () => f('a(b.c)', [{exp: 'b.c', start: 2, end: 5}]))
  it('a[b]', () => f('a[b]', [{exp: 'a', start: 0, end: 1},{exp: 'b', start: 2, end: 3}]))
  it('a[b.c]', () => f('a[b.c]', [{exp: 'a', start: 0, end: 1},{exp: 'b.c', start: 2, end: 5}]))
  it('a.b ? c.d : e.f', () => f('a.b ? c.d : e.f', [{exp: 'a.b', start: 0, end: 3},{exp: 'c.d', start: 6, end: 9}, {exp: 'e.f', start: 12, end: 15}]))
  it('[a]', () => f('[a]', [{exp: 'a', start: 1, end: 2}]))
  it('{a:b, c: d.e}', () => f('{a:b, c: d.e}', [{exp: 'b', start: 3, end: 4}, {exp: 'd.e', start: 9, end: 12}]))
  it('[...a]', () => f('[...a]', [{exp: 'a', start: 4, end: 5}]))
  it('!a', () => f('!a', [{exp: 'a', start: 1, end: 2}]))
  it('a,b,c', () => f('a,b,c', [{exp: 'a', start: 0, end: 1}, {exp: 'b', start: 2, end: 3}, {exp: 'c', start: 4, end: 5}]))
  it('100 + a + b.c + d.e()', () => f('100 + a + b.c + d.e()', [{exp: 'a', start: 6, end: 7},{exp: 'b.c', start: 10, end: 13}, {exp: 'd', start: 16, end: 17}]))
})

describe('Extract fields', function() {
  it('includes', () => e('"hello".includes("he")', {}, true))
})

